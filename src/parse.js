'use strict'

const parseString = require('xml2js').parseString
// const util = require('util')

module.exports = {
  parseResponse: (data) => {
    return new Promise((resolve, reject) => {
      parseString(data, {explicitArray: false}, function (err, result) {
        if (err) reject(err)
        try {
          // parse success
          let services = result['dp:ListProcessesReply']['dp:ServiceList']['dp:Service']
          let output = []
          // console.log(util.inspect(result, false, null))
          if (!services) {
            reject('Invalid results: ' + JSON.stringify(result))
          }
          for (const service of services) {
            let name = service.$.Name
            let plist = service['dp:ProcessList']['dp:ProcessProp']
            // console.log(plist)
            let processes = []
            if (!Array.isArray(plist)) {
              // force the data into an array if it is just an object
              plist = [plist]
            }
            for (const p in plist) {
              let item = plist[p].$
              if (item) {
                // pull out the data values, and normalize the property names
                processes.push({
                  name: item.Name,
                  description: item.Description,
                  status: item.Status,
                  uptime: item.UpTime
                })
              }
            }
            output.push({
              name,
              processes
            })
          }
          resolve(output)
          // console.log('status:', result['dp:ListProcessesReply']['dp:ServiceList']['dp:Service'])
        } catch (ex) {
          reject(ex)
        }
      })
    })
  }
}
