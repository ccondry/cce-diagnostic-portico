'use strict'

const axios = require('axios')
const parse = require('./parse').parseResponse
const https = require('https')
const moment = require('moment')

function listProcesses ({host, username, password, rejectUnauthorized = true}) {
  return new Promise((resolve, reject) => {
    let options = {
      auth: {
        username,
        password
      },
      httpsAgent: new https.Agent({ rejectUnauthorized })
    }
    let url = 'https://' + host + ':7890/icm-dp/rest/DiagnosticPortal/ListProcesses?Random=' + moment().valueOf()
    axios.get(url, options)
    .then(response => {
      // REST success
      parse(response.data)
      .then(result => {
        resolve(result)
      })
      .catch(error => {
        // parse error
        reject(error)
      })
    })
    .catch(error => {
      // REST error
      reject(error)
    })
  })
}

module.exports = {
  listProcesses
}
