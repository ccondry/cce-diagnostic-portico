'use strict'

var expect = require('chai').expect
var portico = require('../src/index')
// const util = require('util')
const config = require('../client.config.json')

describe('Successful response from list process tests', function() {
  it('should successfully list processes from dCloud CCE Call server', function(done) {
    portico.listProcesses({
      host: config.host,
      username: config.username,
      password: config.password,
      rejectUnauthorized: config.rejectUnauthorized
    })
    .then(response => {
      // console.log(util.inspect(response, false, null))
      done()
    })
    .catch(error => {
      done(error)
    })
  })
})
