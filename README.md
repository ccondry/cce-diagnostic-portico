# cce-diagnostic-portico
A A JavaScript library for interfacing with the Cisco CCE Diagnostic Framework Portico REST APIs

## Usage
Here is an example usage of this library:
```js
const portico = require('cce-diagnostic-portico')

portico.listProcesses('ccedata.mydomain.com', 'administrator', 'password', false)
.then(response => {
  console.log('response: ', response)
})
.catch(error => {
  console.log('error: ', error)
})
```
